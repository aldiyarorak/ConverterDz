﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ConverterDZ
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string day = null;
        string month = null;
        string year = null;
        Converter cv = new Converter();
        DateTime dt;

        public MainWindow()
        {
            InitializeComponent();
        }
        

        private void DataConvertButton_Click(object sender, RoutedEventArgs e)
        {
            if(Check() == true)
            {
                string[] data= new string[3];
                day = dayTextBox.Text;
                month = monthTextBox.Text;
                year = yearTextBox.Text;
                Type type = data.GetType();
                data[0] = day;
                data[1] = month;
                data[2] = year;
                object obj = cv.Convert(data, type, ddMMTextBox.Text, CultureInfo.InvariantCulture);
                dataLabel.Content = obj;
                dataTextBox.Text = dataLabel.Content.ToString();
                label.Content = null;

            }
            else
            {
                label.Content = "!";
            }
        }

        private void DmyConvert_Button_Click(object sender, RoutedEventArgs e)
        {
            if(dataLabel.Content != null || dataTextBox.Text != null)
            {   
                dt = Convert.ToDateTime(dataTextBox.Text);
                string[] data = new string[1];
                Type[] types = new Type[2];
                types[0] = dt.GetType();
                types[1] = data.GetType();
                object[] obj = cv.ConvertBack(dt, types, dmTextBox.Text, CultureInfo.InvariantCulture);
                dmeLabel.Content = obj[0];
            }
        }
        private bool Check()
        {
            if (dayTextBox.Text.Length == 2&& monthTextBox.Text.Length == 2&& 
                yearTextBox.Text.Length == 4||yearTextBox.Text.Length == 2 && Convert.ToInt32(monthTextBox.Text)<= 12 &&
                Convert.ToInt32(dayTextBox.Text) <= 31 && dayTextBox.Text.Length != 0 && monthTextBox.Text.Length != 0 && yearTextBox.Text.Length != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
